import json
from aiohttp import web
from aiohttp.client import ClientSession
from aiohttp.web import Response, RouteTableDef
from django.template import Origin


url = "https://pruebamw.vivaair.com"
router = RouteTableDef()
## Validar datos de entrada


@router.post('/consultar_vuelos')
async def consultar_vuelos(request):
    data = await request.json()
    print(data)    
    async with ClientSession() as sesion:
        endpoint = "/availability/simple_availability/"
        respuesta: Response = await sesion.post(
            url=f"{url}{endpoint}",
            headers={
                "Content-Type": "application/json"
            },
            data=json.dumps(data))
        response = await respuesta.json()
        if str(respuesta.status) in ['200', '201']:
            return web.json_response(response, status=respuesta.status)
        elif str(respuesta.status) in ['408', '500', '502', '513']:
            return web.json_response(response, status=respuesta.status)
        else:
            return web.json_response(response, status=respuesta.status)

if __name__ == '__main__':
    app = web.Application()
    app.add_routes(router)
    web.run_app(host="0.0.0.0", port=8081, app=app)
