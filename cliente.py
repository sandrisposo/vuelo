import asyncio
import json
from aiohttp.client import ClientSession

async def main():
    # está es la url base
    url = "http://localhost:8081"
    cuerpo = {
        "origin": "BOG",
        "destination": "MDE",
        "beginDate": "2022-10-20",
        "passengers": {
            "ADT": 2,
            "CHD": 2
        }
    }
    async with ClientSession() as session:
        endpoint = "/consultar_vuelos"
        async with session.post(
            f"{url}{endpoint}",
            headers={"Content-Type": "application/json"},
            #ssl=False,
            data=json.dumps(cuerpo)) as response:
            # Hacer algo con response
            if response.ok:
                print( await response.json())

if __name__ == '__main__':
    asyncio.run(main())